'use strict';
$(document).ready(function () {

$('#btn_maps').on('click', function () {

    if ($(this).attr('data-is_valid_map') === 'noValid') {
        $(this).text('Show GridView');
        $('#show_maps').css({'display': 'block'});
        $('.showGridView').css({'display': 'none'});
        $(this).attr('data-is_valid_map', 'valid');
    } else {
        $(this).text('Show Map');
        $('#show_maps').css({'display': 'none'});
        $('.showGridView').css({'display': 'block'});
        $(this).attr('data-is_valid_map', 'noValid');
    }
});

});

function initMap() {

    var map, coordinate = true, obj;
    $.ajax({
        type: "GET",
        url: "ajax",
        data: {coordinate: coordinate}
    }).success(function (data) {
        obj = JSON.parse(data);
        console.log(obj.coordinates);
        console.log(obj.description);
        showMap(JSON.parse(obj.coordinates), obj.description);
    });

    function showMap(position, description) {
        var infoWindow = new google.maps.InfoWindow;

        var latLng = new google.maps.LatLng(position.lat,position.lng);

        map = new google.maps.Map(document.getElementById('show_maps'), {
            center: latLng,
            zoom: 12
        });
        var marker = new google.maps.Marker({
            map: map,
            position: latLng
        });

        marker.addListener('click', function() {
            infoWindow.setContent(description);
            infoWindow.open(map, marker);
        });
    }
}

