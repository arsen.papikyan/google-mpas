'use strict';

function initMap() {
    show(JSON.parse($('#maps-coordinates').val()));
    function show(position) {
        var latlng = new google.maps.LatLng(position.lat,position.lng);

        var map = new google.maps.Map(document.getElementById('map'), {
            center: latlng,
            zoom: 8,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        });

        var marker = new google.maps.Marker({
            position: latlng,
            map: map,
            title: '',
            draggable: true
        });


        google.maps.event.addListener(marker, 'dragend', function(a) {
            document.getElementById('maps-coordinates').value = JSON.stringify(a.latLng);
        });
    }
}