<?php

namespace backend\assets;

use yii\web\AssetBundle;
use Yii;

/**
 * Main backend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/site.css',
    ];
    public $js = [
//        'js/script.js'
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapPluginAsset',
    ];

    public function init()
    {
        parent::init();

        if (Yii::$app->controller->id == "maps" && Yii::$app->requestedAction->id == "update") {
            array_push($this->js, 'js/map.js');
            array_push($this->js, 'https://maps.googleapis.com/maps/api/js?key=AIzaSyA21Roux94qWqx9rNvVo9_1S6R6Y2VDhLk&signed_in=true&libraries=places&callback=initMap');
        }
        if (Yii::$app->controller->id == "maps" && Yii::$app->requestedAction->id == "index") {
            array_push($this->js, 'js/script.js');
            array_push($this->js, 'https://maps.googleapis.com/maps/api/js?key=AIzaSyA21Roux94qWqx9rNvVo9_1S6R6Y2VDhLk&callback=initMap');
        }
    }
}
